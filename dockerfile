# Usa una imagen base de Node.js 14
FROM node:14

# Establece el directorio de trabajo dentro del contenedor
WORKDIR /app

COPY . .
RUN npm i
RUN npm run build

# Comando por defecto al ejecutar el contenedor
CMD npm run serve

